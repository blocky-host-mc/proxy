from google.cloud import error_reporting

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

from flask import Flask
from flask import request
from flask import Response
from flask import jsonify

from tinydb import TinyDB, Query
import zmq

import random
import string

import requestBuilder as requestBuilder

# Initilize Firebase db
cred = credentials.Certificate("firebase_admin_key.json")
firebase_admin.initialize_app(cred)
db = firestore.client()

compute = googleapiclient.discovery.build('compute', 'v1')
stackdriver = error_reporting.Client(
    project='blocky-host-mc', service="proxy", version="0.1.0")

app = Flask(__name__)

context = zmq.Context()
#socket = context.socket(zmq.REP)
#socket.bind("tcp://*:5555")

servercache = TinyDB('./servercache.json')
servercache.purge()

Query = Query()


def authenticate(token):
    print(token)
    if (token == "demo"):
        return True
    else:
        return False


def translateException(excMessage):
    if excMessage == "not_found":
        return 404
    elif excMessage == "already_launched":
        return 409
    elif excMessage == "insufficient_credits":
        return 402
    elif excMessage == "bad_request":
        return 400
    else:
        return 500


def getServerDocFromServerName(serverName):
    docs = db.collection(u'servers').where(
        u'name', u'==', serverName).where(u'archived', u'==', False).stream()
    serverDoc = None
    for doc in docs:
        serverDoc = doc
    if not serverDoc:
        raise ValueError("not_found")
    else:
        return doc


def getUserDocFromServerDocRef(serverDocRef):
    docs = db.collection(u'users').where(
        u'server', u'==', serverDocRef).stream()
    userDoc = None
    for doc in docs:
        userDoc = doc
    if not userDoc:
        raise ValueError("not_found")
    else:
        return doc

def launchServer(serverName):

    if servercache.contains(Query.serverName == serverName):
        raise ValueError("already_launched")

    try:
        serverDoc = getServerDocFromServerName(serverName)
    except Exception as e:
        raise e

    try:
        userDoc = getUserDocFromServerDocRef(serverDoc.reference)
    except Exception as e:
        stackdriver.report(e)
        raise e

    if userDoc.to_dict()['credits'] < 1:
        raise ValueError("insufficient_credits")

    try:
        imageDoc = serverDoc.to_dict()['image'].get()
        locationDoc = serverDoc.to_dict()['location'].get()
        instanceDoc = imageDoc.to_dict()['instance'].get()
    except Exception as e:
        stackdriver.report(e)
        raise e

    sessionDocRef = db.collection(u'sessions').document()
    sessionDocRef.set({
        'server': serverDoc.reference,
        'launch': firestore.SERVER_TIMESTAMP,
        'user': userDoc.reference,
        'credits': userDoc.to_dict()['credits'],
        'cost': instanceDoc.to_dict()['cost']
    })

    instanceId = 'mc-' + \
        ''.join(random.choice(string.ascii_lowercase + string.digits)
                for _ in range(16))

    instance.create(
        instanceId, locationDoc.to_dict()['datacenter']['region'], locationDoc.to_dict()['datacenter']['zone'], serverDoc.id, imageDoc.to_dict()['imageString'], instanceDoc.id)

    
    try:
        compute.instances().insert(
            project='blocky-host-mc',
            zone='us-central1-c',
            body=requestData).execute()
    except Exception as e:
        print(e)
        stackdriver.report(e)
        sessionDocRef.update({
            'failed': True
        })
        raise ValueError("internal error")

    servercache.insert({
        'serverName': serverName,
        'sessionId': sessionDocRef.id,
        'instanceId': instanceId,
        'status': 227,
        'players': 0,
        'active': 0
    })

    return

def reportServer(serverName, status):

    try:
        servercacheDoc = servercache.search(Query.serverName == serverName)[0]
    except:
        print("not_found")
        raise ValueError("not_found")

    if status == 103 or status == 104 or status == 105:
        sessionDocRef = db.collection(u'sessions').document(
            servercacheDoc['sessionId'])
        sessionDocRef.update({
            'shutdown': firestore.SERVER_TIMESTAMP,
            'activeMinutes': int(request.form["active"]),
            'reason': request.form["status"]
        })
        servercache.remove(Query.serverName == serverName)
        return

    elif status == 228 or status == 229:
        servercache.update({
            'status': request.form["status"],
            'players': request.form["players"],
            'active': request.form["active"]
        }, Query.serverName == serverName)

        if status == 229:
            # socket.send_json({"server": serverName, "status": 229})
            print("ingore")
        return

    else:
        raise ValueError("bad_request")







@app.route('/server/<serverName>/report', methods=["POST"])
def server_report(serverName):

    if not (authenticate(request.headers.get('token'))):
        return Response('unauthorized', 401)

    print('Received report from daemon: "' +
          serverName + '": \n' + str(request.form.to_dict()))

    try:
        reportServer(serverName, int(request.form["status"]))
        return Response({
        }, 200)

    except Exception as e:
        return Response({
        }, translateException(str(e)))


@app.route('/server/<serverName>/launch', methods=["GET"])
def server_launch(serverName):

    if not (authenticate(request.headers.get('token'))):
        return Response('unauthorized', 401)

    print('Trigger from bungeecord: ' + serverName)

    try:
        servercacheDoc = servercache.search(Query.serverName == serverName)[0]
        return Response({
            'players': servercacheDoc['players']
        }, servercacheDoc['status'])
    except Exception:
        try:
            launchServer(serverName)
            return Response({
            }, 227)

        except Exception as e:
            print(e)
            return Response({
            }, translateException(str(e)))







if __name__ == "__main__":
    app.run(host="0.0.0.0", port="8080", debug=True)
