import json
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

def initialize():
    # Set global
    global db, session, metadata, endpoint, stackdriver, wsgi, query, server, instance, billing
    
    # Initilize Firebase db
    cred = credentials.Certificate("firebase_admin_key.json")
    firebase_admin.initialize_app(cred)
    db = firestore.client()

    # Import components
    import app.query as query
    import app.session as session
    import app.metadata as metadata
    import app.server as server
    import app.instance as instance
    import app.endpoint as endpoint
    import app.stackdriver as stackdriver
    import app.wsgi as wsgi
    import app.billing as billing

