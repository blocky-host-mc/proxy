from app import init as app

import requests

# dc_raw = requests.get("http://metadata.google.internal/computeMetadata/v1/instance/attributes/serverId", headers={"Metadata-Flavor": "Google"}).content.decode("utf-8")
dc_raw = "projects/493691091520/zones/us-central1-a"
dc = dc_raw.split('zones/', 1)[-1]
region = "-".join(dc.split("-", 2)[:2])
zone = dc.split(r'-')[-1]
