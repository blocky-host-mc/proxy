from app import init as app

from gevent.pywsgi import WSGIServer

http_server = WSGIServer(('', 8080), app.endpoint)

def serve():
    app.endpoint.flaskapp.run(host="0.0.0.0", port="8080", debug=True)

