from app import init as app

from google.cloud import logging, error_reporting


logging_client = logging.Client(project='blocky-host-mc')
logger = logging_client.logger("proxy-daemon-" + app.metadata.region)
error_client = error_reporting.Client(
    project='blocky-host-mc', service="proxy-daemon-" + app.metadata.region, version="0.1.0")

def report(e):
    error_client.report(e)

def log(text):
    logger.log_text(text)
