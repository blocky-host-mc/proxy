from app import init as app

def getServerDocFromServerName(server_name):
    docs = app.db.collection(u'servers').where(
        u'name', u'==', server_name).where(u'archived', u'==', False).stream()
    server_doc = None
    for doc in docs:
        server_doc = doc
    if not server_doc:
        raise ValueError("not_found")
    else:
        return doc

def getUserDocFromServerDocRef(server_doc_ref):
    docs = app.db.collection(u'users').where(
        u'server', u'==', server_doc_ref).stream()
    user_doc = None
    for doc in docs:
        user_doc = doc
    if not user_doc:
        raise ValueError("not_found")
    else:
        return doc