from app import init as app

from tinydb import TinyDB, Query, where


server = TinyDB('app/data/cache/server.json')
server.purge()
Query = Query()


    
def new(server_doc, user_doc, instance_doc, instance_id):

    if server.contains(Query.name == server_doc.to_dict()['name']):
        raise ValueError("already_exists")

    try:
        session_doc_ref = app.db.collection(u'sessions').document()
        session_doc_ref.set({
            'server': server_doc.reference,
            'launched': app.firestore.SERVER_TIMESTAMP,
            'user': user_doc.reference,
            'hourlyCost': instance_doc.to_dict()['hourlyCost'],
        })

        app.session.server.insert({
            'name': server_doc.to_dict()['name'],
            'sessionId': session_doc_ref.id,
            'instanceId': instance_id,
            'statusCode': 227,
            'playersOnline': 0,
            'activeSeconds': 0,
            'userCredits': user_doc.to_dict()['credits'],
            'hourlyCost': instance_doc.to_dict()['hourlyCost'],
            'userId': user_doc.id
        })

        return session_doc_ref.id

    except Exception as err:
        print(err)
        app.stackdriver.report(err)
        raise ValueError("internal_error")


def exists(server_name):
    if server.contains(Query.name == server_name):
        return True
    else:
        return False


def get(server_name):
    try:
        return server.search(Query.name == server_name)[0]
    except:
        raise ValueError("not_found")

def update(server_name, data):
    app.session.server.update(data, where('name') == server_name)

def terminate(server_name):
    try:

        session_obj = app.session.get(server_name)

        session_cost = app.billing.cost_calculator(session_obj['hourlyCost'], session_obj['activeSeconds'])

        session_doc_ref = app.db.collection(u'sessions').document(session_obj['sessionId'])
        session_doc_ref.update({
                'shutdown': app.firestore.SERVER_TIMESTAMP,
                'activeSeconds': session_obj['activeSeconds'],
                'sessionCost': float(session_cost),
                'cause': session_obj['statusCode']
            })
            
        server.remove(where('sessionId') == session_obj['sessionId'])
        return
        
    except Exception as err:
        print(err)
        app.stackdriver.report(err)
        raise ValueError("internal_error")
    