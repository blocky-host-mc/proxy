from app import init as app


def launch(server_name):

    if app.session.exists(server_name):
        return

    try:
        server_doc = app.query.getServerDocFromServerName(server_name)
    except ValueError as err:
        raise ValueError("not_found")

    try:
        user_doc = app.query.getUserDocFromServerDocRef(server_doc.reference)
    except ValueError:
        app.stackdriver.report(
            "UserDoc not found from server " + server_doc.id)
        raise ValueError("internal_error")
    except Exception as err:
        app.stackdriver.report(err)
        raise ValueError("internal_error")

    if user_doc.to_dict()['credits'] < 1:
        raise ValueError("insufficient_credits")

    try:
        image_doc = server_doc.to_dict()['image'].get()
        instance_doc = image_doc.to_dict()['instance'].get()
        session_doc_ref = app.db.collection(u'sessions').document()
        session_doc_ref.set({
            'server': server_doc.reference,
            'launched': app.firestore.SERVER_TIMESTAMP,
            'user': user_doc.reference,
            'hourlyCost': instance_doc.to_dict()['hourlyCost'],
        })
    except Exception as err:
        print(err)
        app.stackdriver.report(err)
        raise ValueError("internal_error")

    try:
        instance_id = app.instance.create(server_doc.id, image_doc)
    except Exception as err:
        session_doc_ref.update({
            'failed': True
        })
        raise ValueError("internal_error")

    app.session.server.insert({
        'name': server_name,
        'sessionId': session_doc_ref.id,
        'instanceId': instance_id,
        'statusCode': 227,
        'playersOnline': 0,
        'activeSeconds': 0,
        'userCredits': user_doc.to_dict()['credits'],
        'hourlyCost': instance_doc.to_dict()['hourlyCost'],
        'userId': user_doc.id
    })


def shutdown(server_name):

    if not app.session.exists(server_name):
        raise ValueError("not_found")

    session_obj = app.session.get(server_name)

    try:
        app.instance.destroy(session_obj['instanceId'])
    except Exception as err:
        print(err)

    app.session.terminate(server_name)


def report(server_name, body):

    if not app.session.exists(server_name):
        raise ValueError("not_found")

    statusCode = int(body['statusCode'])

    app.session.update(server_name, {
        'statusCode': statusCode,
        'playersOnline': int(body['playersOnline']),
        'activeSeconds': int(body['activeSeconds'])
    })

    session_obj = app.session.get(server_name)

    session_cost = app.billing.cost_calculator(
        session_obj['hourlyCost'], int(body['activeSeconds']))


    if statusCode == 229 and (session_obj['userCredits'] - session_cost) < 0.1:

        try:
            user_doc = app.db.collection(u'users').document(
                session_obj['userId']).get()

        except ValueError:
            app.stackdriver.report(
                "UserDoc not found from server " + server_doc.id)
            raise ValueError("internal_error")

        except Exception as err:
            app.stackdriver.report(err)
            raise ValueError("internal_error")

        if user_doc.to_dict()['credits'] == session_obj['userCredits']:
            app.session.update(server_name, {
                'statusCode': 402
            })
            app.server.shutdown(server_name)
            return

        else:
            app.session.update(server_name, {
                'userCredits': user_doc.to_dict()['credits']
            })


    elif statusCode == 103 or statusCode == 104 or statusCode == 105:
        app.session.terminate(server_name)
