from app import init as app

from flask import Flask
from flask import request
from flask import Response
from flask import jsonify


flaskapp = Flask(__name__)

def authenticate(token):
    print(token)
    if (token == "demo"):
        return True
    else:
        return False
    
def translateException(excMessage):
    if excMessage == "not_found":
        return 404
    elif excMessage == "server_exists":
        return 409
    elif excMessage == "insufficient_credits":
        return 402
    elif excMessage == "bad_request":
        return 400
    else:
        return 500


@flaskapp.route('/server/<server_name>/launch', methods=["GET"])
def server_launch(server_name):

    if not (authenticate(request.headers.get('token'))):
        return Response('unauthorized', 401)

    try:
        app.server.launch(server_name)
        server_obj = app.session.get(server_name)
        return Response({
            'playersOnline': server_obj['playersOnline']
        }, server_obj['statusCode'])

    except ValueError as err:
        status_code = translateException(str(err))
        return Response({
            }, status_code)


@flaskapp.route('/server/<server_name>/report', methods=["POST"])
def server_report(server_name):

    if not (authenticate(request.headers.get('token'))):
        return Response('unauthorized', 401)

    try:
        app.server.report(server_name, request.form)
        return Response('ok', 200)

    except ValueError as err:
        status_code = translateException(str(err))
        return Response({
            }, status_code)
