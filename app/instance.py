from app import init as app

import googleapiclient.discovery

import random
import string

compute = googleapiclient.discovery.build('compute', 'v1')

def create(serverId, imageDoc):

    imageString =  imageDoc.to_dict()['imageString']
    instanceType = imageDoc.to_dict()['instance'].id
    instanceId = 'mc-' + \
        ''.join(random.choice(string.ascii_lowercase + string.digits)
                for _ in range(16))

    data = {
        "kind": "compute#instance",
        "name": instanceId,
        "zone": "projects/blocky-host-mc/zones/" + app.metadata.region + "-" + app.metadata.zone,
        "minCpuPlatform": "Intel Skylake",
        "machineType": "projects/blocky-host-mc/zones/" + app.metadata.region + "-" + app.metadata.zone + "/machineTypes/" + instanceType,
        "displayDevice": {
            "enableDisplay": False
        },
        "metadata": {
            "kind": "compute#metadata",
            "items": [
                {
                    "key": "shutdown-script",
                    "value": "curl http://localhost:8080/shutdown"
                },
                {
                    "key": "startup-script",
                    "value": "cd root/server/daemon/; python3 daemon.py"
                },
                {
                    "key": "serverId",
                    "value": serverId
                },
                {
                    "key": "image",
                    "value": imageString
                },
                {
                    "key": "serverName",
                    "value": "niehaus1301"
                },
                {
                    "key": "startcmd",
                    "value": "none"
                },
            ]
        },
        "tags": {
            "items": [
                "minecraft-server"
            ]
        },
        "disks": [
            {
                "kind": "compute#attachedDisk",
                "type": "PERSISTENT",
                "boot": True,
                "mode": "READ_WRITE",
                "autoDelete": True,
                "deviceName": instanceId,
                "initializeParams": {
                    "sourceImage": "projects/blocky-host-mc/global/images/minecraft-daemon-v1",
                    "diskType": "projects/blocky-host-mc/zones/" + app.metadata.region + "-" + app.metadata.zone + "/diskTypes/pd-ssd",
                    "diskSizeGb": "10"
                },
                "diskEncryptionKey": {}
            }
        ],
        "canIpForward": False,
        "networkInterfaces": [
            {
                "kind": "compute#networkInterface",
                "subnetwork": "projects/blocky-host-mc/regions/" + app.metadata.region + "/subnetworks/default",
                "accessConfigs": [
                    {
                        "kind": "compute#accessConfig",
                        "name": "External NAT",
                        "type": "ONE_TO_ONE_NAT",
                        "networkTier": "PREMIUM"
                    }
                ],
                "aliasIpRanges": []
            }
        ],
        "description": "",
        "labels": {},
        "scheduling": {
            "preemptible": True,
            "onHostMaintenance": "TERMINATE",
            "automaticRestart": False,
            "nodeAffinities": []
        },
        "deletionProtection": False,
        "reservationAffinity": {
            "consumeReservationType": "ANY_RESERVATION"
        },
        "serviceAccounts": [
            {
                "email": "493691091520-compute@developer.gserviceaccount.com",
                "scopes": [
                    "https://www.googleapis.com/auth/compute",
                    "https://www.googleapis.com/auth/devstorage.read_write",
                    "https://www.googleapis.com/auth/logging.write",
                    "https://www.googleapis.com/auth/monitoring.write",
                    "https://www.googleapis.com/auth/servicecontrol",
                    "https://www.googleapis.com/auth/service.management.readonly",
                    "https://www.googleapis.com/auth/trace.append"
                ]
            }
        ]
    }

    try:
        compute.instances().insert(
            project='blocky-host-mc',
            zone=app.metadata.region+'-'+app.metadata.zone,
            body=data).execute()

    except Exception as err:
        print(err)
        app.stackdriver.report(err)
        raise err

    return instanceId

def destroy(instanceId):

    compute.instances().stop(
        project='blocky-host-mc',
        zone=app.metadata.region+'-'+app.metadata.zone,
        instance=instanceId).execute()


    return instanceId
