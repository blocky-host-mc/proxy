# Import and initilize app
from app import init as app
app.initialize()

app.wsgi.serve()